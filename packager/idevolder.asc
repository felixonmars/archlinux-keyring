-----BEGIN PGP PUBLIC KEY BLOCK-----
Comment: packager: idevolder (DB2277BCD500AA3825610BDDDB323392796CA067)

mQINBE6yuvYBEACur8WU+4wwMMydKHfHtA6Oh6m1Joev/5LKG5KpwZzjeYiiMcqz
f4xhaS2aFY3o15X+VX25GpYbHHOCcrpnkLdkmn/xX/gRJV2fq9OBzypHAfshvlF/
rtLi2LaaE7QlEBJ8T5ijRVK0acZA5Pzr0uMhhL3ioIbqzYpJ0tzJdXQee2nfyGGR
cYJ/JDBge3ppwk61piEEd1KhW3O3w007PNY9sbN4a5MvuDCtIt6CAG0chIMEkfn6
L9gCFtz9uD2+0vIdDzB8t2+wN58OPIdMyTnejQ0/P+vsODuTH6+RIGgFZ3qwCaxt
gNKzyh4asGhzpsSNgvSH+T66bp30CsQE9veVHbbzJfHCGQ1cLBz2qCa6qyUYpeqd
3CH+E3LxRfpyhYB55MheXkY1Bh+8k6TmxA3LsIxCidE1zSNwhmKvrsEkY3WCMMMC
cA2+7FE0RdjYJzhp7H6SXrXeke86loRFDzL+c0NQRXE8giWszQzgz2QiGkdoil61
dyQCB5Q0ri07tHD8VOAml6LLl2INBIwP+zT7AtlPS5sryQxPCBykEpobVg9yPtyd
EE71WVpgyaFpYr4EwDzQec2AmNW8rVVHv4ymd0jmoor3ggrhIMwt9gWOOG/o+Tob
c6VrdQGHYwLEUFULEaiTWD4y5DXVdlMV/4wTyvIxxEBQavsbk0+6J3ov4wARAQAB
tCVJa2UgRGV2b2xkZXIgPGlrZS5kZXZvbGRlckBnbWFpbC5jb20+iQGbBBABAgAG
BQJPZ5JlAAoJEDNIiC9qxqTCha8L+PbKSk8QGudl7iQ1TVXrHknKJJzINETKvtPb
GnPru5DAoy+b+KAtLVsFnXR/TDBeRhtPVurrtZNvP70oPTYwq75oTjjx0/MfSbHN
gkZQZJTcGs1hVe+jKyyt3q7WG7MJkPDzMk4TjL8FWiHvASIs4h54B9iiixO0IWqQ
hppE8onPDXMEpyE/iXs4Qm9t50GHo9Ki1KWw+kbPdvqVF+nF7Kd6sC+oqYz+YntB
2aXjSWFMSyc6l9H6lyhf4o3TOV0sdqopZvBYyQK0661UNydhAcvnr391rIhnEVpp
JSu6N9D0NjKYgvJtI1km+w4pcrtbi50Qur0JE/ucaHxBPYMAVKQlxC4NcqH4l5G2
xxt8jE2mc0EM9X42WtYmMf9RtsJTEBlSXvU7aAPo0KlkBgg86PLFpYiwaO4C2YkF
AZO3W3ClfTD+Vci/DHd/+RTDrE0ZNk3EfdU3n5w0aJCdXnSHT0Dxp817zUobqppN
cjPK8IK6zm+yZN+Cms6ah56Q/dgOiQIcBBABAgAGBQJPaPrPAAoJELod+2T/+Xnn
E8oQAI/KTswwCp24LUSBrxDdNW4R3jI+mwOf8ZvTD4HkzBC1OsADRoTLCqqm6JfJ
4TdtQsXM6irEmQWECOyDJmhOljy62/MZZYgikON79+UIgbMxGyI/v2SL1Wq0Xvzt
JtzdChcq/xHxV2lmufwBJSVZ7Msv4jQeurhtHkDcjHEZQOZxsYy3etNYtTHDOCFt
p8Rf7J/bqDInmN7HYbMmKjbIsKn6zda86M1+4YkwEJQGPWI/MBKvGUCW4AsOynmc
Yk6bevAQStyXrSLB/6Sbb7DLWb++OHXqZSqc4H8yvny1/vuFJJ31cndL78arMget
22vVKlDFamX1Qm885XRFj0qY3b7916G/oHR+MKbYEzUd4wZLn8Y9wNjU//fCoHKh
LyPduGraSudvojb+W1i/Bpzel9tq23CS9Lc+l397aCgXz3UK7AlFTgdnB8hwEMOs
jFxeuhTe21BOLltHU1QLpprciF3xZlQW/U+unAEgZpMFaguoUL2tKuPVDrdXKKH+
78cswVZ0/YSc9D5mlXpsEy7AQ0XqkdEEuQCvniOKDaPySVUX9c1rujGB/xJuK985
scpdfePBIOLi226OJVzVvTkO/UQGJnXc7P/wZBT55G6v7eNFEcywd/kkCzEhVOeM
jjY9BKqv3UErWmsEEYgtZFXlZsO+FdugowpnMQe1kgEtUK+0iQIcBBABCAAGBQJW
pJRGAAoJEKiOI+N3UU4AFxsP/1WY+oK6+G80eWy393fJaEpJ4mD7MtV8P5jDVCqp
knD2gd9GEVnlEMIZvPf0AnjDwJgCnuPn27S3uL/izURQdAtU/ldMR4y8+lklMcyG
38o/hWRTMPuEL8xjXLXxgXiepUw6m+Sox2HjVLE1TJ0snHu30CLtoyKzoMuuYjDh
wA2NYa5xGWsFR7UZgwy65VlJcc7LqZata1gg3t4m7QDPfCWWIPxkjX4ltsmKqAOv
tSnhgdl9gmwD0nzWcsQlWSCEqPL5Bmtxa9mekqBUow5cMuv8Ipu7XEj5grE3xsO9
jz4ZUFiGs8cIMvWNpCmMcgGHtoTQWgCSk/dQpqQrKhuRHNL1yLDrd9Vd9gSNyfSH
78+4znEyK5hb+elfW4jLNoWcdFUHsoZdwLX4blo2uOldxGGIhm6lb4CbvyMEJOZS
RSNTjbEYAi6QLgaJq2yup1sz8i3MYaofpIwBWN+vHn6F11+RaPAKwre4H3Zfs+e2
Ozgsl8K7IglqKJj1Uys8KjvDMgEZjNta/Gu1qBkd+ZEObA0sPerGlOcmjtGkpVlM
nf6cleb0/aaGvJly/yE4oZycE4ZcBh4V6fAJOmpwYjR9u0i9DtS+T/iqv9iY9w0o
lYvxoRLpvaJNYzDIU8K5vCIMELoNdkKGHPwSoi+k8cHlU/+kOh207IdqjzGeevWp
iBtCiQI4BBMBAgAiBQJOsrr2AhsDBgsJCAcDAgYVCAIJCgsEFgIDAQIeAQIXgAAK
CRDbMjOSeWygZwnFEACfWtOyLqlmoN6iwS7Sc51PCQDeSHHwNxxwKIF/e+xdrea5
0m0GcodAw/sFUb+OAVAe6B0FbV/yQv4ez/4u5GhHgee14XhA0iqDppzgTtkgbEo/
Y4+FU5n6PoUJ5t1Loi+x0laIl6vkYHbq0DqBEIz+dlncd3umGMmHIyrT5GwIA2s/
BjvVOJEiBLG1kl8Tgtn5HczfQHUST+dYnWoEbuQpqWzUEAkl6UoBSvTX9Kd+jGiS
vnJa7o/qh7qDVNT5GEt6ujcEAtRE0lgjmyl83kNaHFnZx1xU0d0t/Fgz5i/2aSyd
DRxj8I421GGI5ju1n4HSVRFdoIOm9+qqmiNNSSldMtErBGos2j5lYokVhVXqN79S
5/W6PEcvTkX0Ob7XWPk6OFMoe0fK7Q2aIP3b+urRC1Of1Z0hBn82Y0/FCo7JK0jz
E+55e93kuPcaEYd6V4ISFpqOKgSDumSUz7eldPXRIgPbAm2z1h+pTnjHvkIuq5tP
JUvBg7ayggGlDgqVL55IBOAwPjAboa0wf+kt9lZudYRK98u6aXuyVwIcoWSlLyHS
SHhstEKzx0sEPLRbMN+V4J27Us08Cntd8tEDgbrTYKKYmjP9pwWL651AyIex6pFC
nr9400OIWqml6KoxLzd75ZNufeciAZB+1jDM4F/52QBbiKi+SJOCf8eDUKFHQYkB
HAQQAQIABgUCT4atqgAKCRDy27STGYWpkruiB/9nRuDKjpj+++/HYc6hvt+fgRKw
CCbxjYC3wBazvyhhL3ltPg2XmOPUkP82oBKn0JvLefg8/6GXbIoAvg+zCYIHUR+u
WjqGjbP9NiQJtbeuU0ENNHLlNY1+Bo3bhcFX2WHOcmqctPUj9bjpdWe55jtJC4RW
Q91zId+b9YbfVbEkSVPAHJbRffIIaR1+bXWNqL1l3OFFWt1gesGdNlKWVnD6Sf87
0jl1X0M13X31M0UTMNrDF+TOf2i91H+x7SoBLJJxS6WKbyPzQTNVfnXeqdkwziFF
DAOqBNS6RKG09RIWg1gWqnxA52GDwG2twI5IDeN2Hd+G+rv4t3EJTjjuUpseiQIz
BBABCgAdFiEE2K/doHpbbt+n2Mza1tBV+SeEPxwFAl2Q4f4ACgkQ1tBV+SeEPxzr
VA//c3Ofiju2gVSIAThkvIcfnWXwbvhRfRaVue/v8fuyCaZn2lRCoLIi4V2OJ2IL
MHwvPqoj41rcurNp13nghWXcKV1NGINPyxyNodMx97jKJ1BKFHUKo15Rxuu0uat3
mKcKYQ9Jug3v1Sf36ucZ9AvY81CT9CYC0BF9ZrPHFsncLmtlf+TueF695HPc0uO6
NmZA9Kn91dc/U8DXlYj71s1kN6xVrgueYzEOF7eGpmc5KrtiGnbU8adp7EcHALp2
Ba33ZnreN8aw1CgqXZwnYqUxcZZWnYUCvhMztijoPZyun4G+yvjaPmET3leoRZPJ
h1xOeTS/lhQ30Z7lq/IiznCZMQShrMj+hL2ag2rR/rubgR8kKBsPZRDRro+EfDgk
dww9vtVjcJn65hS1cZ5Fhuf6ce0CUDKKDqB3JpqDJy4cZ9w2vShOMTwbkAsb7R+5
ussxCTjpea1KJg61tjph36oO1DLZ8Ccp9GxvH6V/acBmoroOQL7YS0se4ouiiycI
nGi67SVEByaDtLPq5HJ+5oUHkcF3EhR3vd8NcSZNX8D5KtEnOAEI6dVDRJGgSI9Y
c9zaNVDL3qtXdTQli2Zn7QjhoEiBGFQ9lukft38dBObECIpycKV+4X7o3jOWI+ph
Jr8Yvn+KsOEXme4JmcaOrtvtMr3gt8DczRRAhUHJUAJK75q5Ag0ETrK69gEQAJGS
KQeQ5dSSl7P1jS/KUjXA2Ohl4+PJmyn/PEVyx6iGTyRwl5HB3j3K27jY00onan9T
ZEigsg1PFNwpk8jMasLTrxUISNo/foK85zUNKqcrAz/VNiy1l7Bl33r7srRd+Seg
ILl+Wmlqjcx0caYmN7PLiz/APs5vGGOdWgXwR3JzhvN544Rr9kyBtduClJV7fTFv
6GpETzv4qyyqOejHmodj32aRIgyDcVwQf1aCxBJe7lyIFN5i0/9kLVpI22zcSpTQ
9ZFz2HIhCbLcusuUNdMPmeuivuqPbtF1++csvBU2DTv56MiyqJ1iEvvM7K90hZGS
YYEO7ExiKp8gsSZmxPWuxFGBZq1fheFwVCgwqXrW/y4DZd74d1PUiENnnyS0go3X
yvgshW9+rSgDpKCnFS3A8+f8ML+ewt/8shwbGjBsmPc0ogVcGf4R3IHTfT8Y+g0o
Gi+Y+JSwXyfxoB4hPXFaP83RIxSFuw8yS6satkmGSAzdC27pyLjRElJlVFb8HBP9
I9duTA1GIp3vJv0aeYOL0cwVzCJJjQEhGx53sggHGLAgKNxrfoD068hfULQ1m12e
JE3lhWSFhIX1oYi45/4pZ1k9nWghvc70ivkXn30d0wltJO8W0O/3QbAYyJtQqRE6
PNm3gVZFAuBDmsv1eGjaV/V5fB+hHzD89l8klGzVABEBAAGJAh8EGAECAAkFAk6y
uvYCGwwACgkQ2zIzknlsoGeIhA//bEm8ps1kXG98L4bHcmhNpak5+X3gn69h/TNY
agXK0xHXWtvr3GvPTvgpGmILanxPgrG9PX5xCscK6nIQtAPFmK4BEWGM39BqoOGm
bYpil7cp+zE/HfkGA8h2jv3Z/Ol8WoSMLvnsfWV9tSC4FHz+YPct7lLrRfOpKmzB
gvdp4gDY3MulZqKwS1wk/1YlZ34LdrvbtuAi7k3gp6cZrLJgkISen3BQrUaYKklx
y/w7OUlnpkprDwkZ6okL0FQEJQHS6tlGAJbNVxLIUHQw9qe9IyY8DOjG3pgv5o2g
v/HJiFU+ggHBYG5gSttLiISljGkZlN1H5gwuohrS+vNL2gPtV3HPRw6NB/N1VtKD
BvQqJ2kzX6ap8H5XCCqeAwo2V1phxoBj7XeurkSqYz8tsg4BmbI7DeRZylgWWeS9
qL2Re/tmXCpvCvSXf7GGbJJJUtH/50vTup4nWVxgHaMM22lUjyaUUqGpK/TJ5g32
py5ExTcvbrc9L9RrxNDy22CFiHEXRoX4xTR1LbwPF1bEzq67JZc2500xLCnafu0j
LIomtaWwguOcrE6HHjIJbnpbezfkY7E/KttDfyTG5jKMpuPMHaMR85WCY8AZRXoU
aj+HEEjX5XVEoFmzxROFIzt3mjLpuCJPaMAWBqkvLCBcRb2cVU0fSLHvFNNMpjO+
N2S85j4=
=Ixql
-----END PGP PUBLIC KEY BLOCK-----
-----BEGIN PGP PUBLIC KEY BLOCK-----
Comment: packager: idevolder (531ECF3644A44FEA0B47DBCDE1E3CF0539448BFF)

mDMEYX1PhhYJKwYBBAHaRw8BAQdAnHxwWORZt8WFgn3JvufWo3o6OWJggEUe71Qa
pOC1nma0KUlrZSBEZXZvbGRlciA8aWtlLmRldm9sZGVyQGFyY2hsaW51eC5vcmc+
iJYEExYIAD4WIQRTHs82RKRP6gtH283h488FOUSL/wUCYX1PhgIbAwUJA8JnAAUL
CQgHAgYVCgkICwIEFgIDAQIeAQIXgAAKCRDh488FOUSL/7giAQD/A0ncQbW7vZ7t
99WD8+fo2DsipFnxeA9jV2Ao0gLq6QEAvTYR560R7zMJlwkyFj+DANm/ST7lipdL
BBrYnf1GEAKJAjMEEAEKAB0WIQSR/+BwDoBhnOtzI1yojiPjd1FOAAUCYX/pBgAK
CRCojiPjd1FOADf5EACUAnFKutZG3tOTPSO30KZo3in1SwmVCs/vjPemdddqM1zd
b2jR8+lCSPV/17p/Sn+cBqvUNGWNZqQKta28zaEMbO6VdvBArD+XtvXX0zgPX0h/
0UdK3z9WUGVUfIliSQ01dsoyAiIqV8YIvN1nTDm5vHYjRJPoyZNuh48bA9VvBDa0
ifNOMsAePA9fJ6Fulp4vYUOZug2TaHWrC8JPllTzM/6sjII+2LnX88Nv5JXZHcQf
EYVRYHRkOY53glNwJnhQBNoTCJ4s+nQ8QyON7dzwZub6PcUd6aF9nUO9eiQjEdeu
fBNHbjbglT6O2n7cxeQdZtnf9Xfl2iZbZ1FPpqA6+joptzuniCr3uVRcf1kkxntg
7bQSMQO7UZE/1xHQCH/qpLhG/pQ41s7t+LETRxhWtIMf0BL29GHswXIrMDWKGKcC
LVwqXbmbi7urVZHdXFyRHByFIPnJcmMD7zpoxKUXg7R3r5qKuOF0cZe3NNgGYQWN
Pbp/ar/uX3mCCLj5SsUq9n4K3ex8y8kktoE8SgTj5iZXnp/PNdXkwg4yMSs4iF++
w/os7j5ZqT7hkffUK9mLeo4GKSFazSbTtVVd5epq8zzogFKjxEGxV/AibZj+wAkO
iNd+m6B7HaubxVB9RsouJXbe9TDIbNVK5LhuhMkHbTVWjGMS8Em6ULvPXR+ENYkC
MwQQAQgAHRYhBNsid7zVAKo4JWEL3dsyM5J5bKBnBQJhf/AMAAoJENsyM5J5bKBn
WtkP/15fNTwXgrI77xgwS6iQCEFVCLs9gd5CzElzqcKMfkGaP31JJh0WLjbFBb8U
5pTcIaSpeV+633TIRzlw1oqWdYh3B2rYXbOSu97T7OP10MAxDIQVx+ha0vPjAUVz
gVeIwJPToXbGjKLKEyMVedIbdGavLEwMG8hORaCHPCQFZ/Fa07/pBenHlUKvsW7z
xSZZ/jlB/OxfLSr00w5PaVYXy9AXIQtjqoNb/GQ7R4hwN6MdkymJEriobmpkOkdV
NYz1b0jjqZ9EwlFkNVcCg9S6EyeCHVLO7Ak0npHdeFCwety5D6/I6V0UeejSYQbR
8rTqPYp9uMO9rj4oW0POrAJP8dJwrfmVf5v/Sg3gIO4AQD/g+aZ+T9HJKOjuvhfp
Dw1oJPHpQmcy7m3k6L7RbCJuOb0oEFOIxV8zqBshNIpTUNeDs5YYP/Cy+Q6jiThj
730JGoVF0PBggQxHDD/qA+mmzVlJsZA5MV6AjdkrYUjFRcfsdO6rF538qfDkjWjh
7BTg77z419ATUCaiu5fNQtjtPtunP6CjRwJbMJ9TmS2ydjbQcnylOSEFkbPZpcG8
+NNWnZK7oLE+hbHzGknJMyvN4ilCUZQ++LPCyMP49NIDDzLLBx2tI3dsOW4dDbrK
FBfMsTFZzhGvvM4Q2JrtB+C7WIpzVLdyjMVRRwjlLNn+dB3ZiHUEEBYKAB0WIQQq
wKQu+wtcvHoEAu1NyVtte+mJLgUCYYBktAAKCRBNyVtte+mJLqQkAP45knFKVllz
JGhFXrUWQBcj3SuZ2/gD+6+B0sVoXXMPWAD+MlZHI+n0pwGuJ+27whpAs4hE9fPT
vN00Pt9fG0n7aQGJAbMEEAEIAB0WIQQOi2RAefWZ38Hdw5czSIgvasakwgUCYYej
lwAKCRAzSIgvasakwlalC/9z9x1b5DUkcJOupr6gBB7HMCQJjpI9bJj7H6JikURt
nq6fFQaDDPOTfYWe01EDRAWhqw/hqhae5fJ3ITS8f0FY5DCi+QLytpbzvFQ0yzqQ
5oWvc7RD76nE5kJqTG2JtRUtUKYjS2VAnEY3p42OAwq0KRRKbOTzdviahXrqP6w1
HJaV4JNhusfpJkRVcFWxv7VhzSYgcKZQPGDiiX67aP0RlnINjXyS/MTYQY6DbJwh
Kps/hFZtyG+HdqfUEG9QdYeWfIGIWId8XXZfVih6XloeulVekeEJmQV9qbLYlF7u
ivxfnDlHcXZf4Mfi5yGYu93v+oJj8Fy80TG1V5LkFute2a6VZeD0P5teTo5iY5/a
q7Qe0P0mbqoMfYm1dWtvaoVnMU4LUAI5C7ZCV7HFilvV28z7mfFiTsAjmVudLdjX
K+wih9GdSCuOb7VgkpX1y1amsVj2aziLfFLoAtUCUzLDLa0pDU8c+j6SJ9b4jcyX
y0dcLCSq6P7wI84EOirpey+4OARhfU+GEgorBgEEAZdVAQUBAQdAYTwvs24o0xzM
gj9qwYTGA26IkFoMheIOx1ZyuEVXJEgDAQgHiH4EGBYIACYWIQRTHs82RKRP6gtH
283h488FOUSL/wUCYX1PhgIbDAUJA8JnAAAKCRDh488FOUSL/3u8AP0ZVYq51XlW
jkL1D+acZMbzRyAe0s400Zcdb+H1qS2TVgEAxC8l7WIFYffW60ZZ2Aqg7uvzYlgi
2KcxgDIKStJsuwo=
=AT+T
-----END PGP PUBLIC KEY BLOCK-----
